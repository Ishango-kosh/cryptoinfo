const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost', {
    user: "root",
    pass: "password",
    dbName: "scrapping",
    useNewUrlParser: true
});

module.exports = mongoose.connection;