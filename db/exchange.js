const db = require("./db")

const mongoose = require("mongoose")

var ExchangeSchema = mongoose.Schema({
  exchange: String,
  name: String,
  price:String,
  currency: String
},{
    "Collections": "exchange_data"
});
const Exchange = db.model("exchange_data", ExchangeSchema)

module.exports = Exchange;