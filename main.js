'use strict';
 
const express = require('express');
const path = require('path');
const app = express();
const port = 3000;
const router = require("./router")
const persistAPI = require('./persistapi')

persistAPI.dropDatabase()
persistAPI.saveBitfinex()
persistAPI.saveCoinbase()

app
    .set('views', path.join(__dirname, 'views'))
    .set('view engine', 'ejs')
    .get('/', (req, res) => { res.render('main'); })
    .use("/exchange", router)
    .listen(port, function(req, res){console.log(`Server is running on port: ${port}`)})

