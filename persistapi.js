const requests = require('requests');

const Exchange = require('./db/exchange')


function dropDatabase(){
    Exchange.remove({});
}

function saveCoinbase (){
    const coinbase = "https://www.coinbase.com/api/v2/assets/summary?&base=EUR&filter=listed&include_prices=true&resolution=day";
    let data="";
    let exchangeCoinbase;
    requests(coinbase)
    .on('data', function (chunk) {
        data +=chunk
    })
    .on('end', function (err) {
        if (err) return console.log('connection closed due to errors', err);
        const result = JSON.parse(data);
        exchangeCoinbase = result.data.map(item=>{
            return Exchange({
                exchange:"coinbase",
                name:item.name,
                price:item.latest,
                currency: item.currency
            })
        });
        Exchange.insertMany(exchangeCoinbase);
    });
}
function saveBitfinex(){
    const bitfinex = "https://api-pub.bitfinex.com/v2/tickers?symbols=ALL";
    let data="";
    let exchangeBitfinex;
    requests(bitfinex)
    .on('data', function (chunk) {
        data +=chunk
    })
    .on('end', function (err) {
        if (err) return console.log('connection closed due to errors', err);
        const result = JSON.parse(data);
        exchangeBitfinex = result.map(item=>{
            return Exchange({
                exchange:"bitfinex",
                name:item[0].substring(1, (item[0].length-3)),
                price:item[1],
                currency: item[0].substring((item[0].length-3), (item[0].length))
            })
        });
        Exchange.insertMany(exchangeBitfinex);
    });
}

module.exports = {
    saveBitfinex,
    saveCoinbase,
    dropDatabase
}

