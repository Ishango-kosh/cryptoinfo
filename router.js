const express = require("express");
const router = express.Router();
const Exchange = require('./db/exchange')



router.get("/coinbase", (req, res)=>{
    Exchange.find({"exchange": "coinbase"}).then(data =>  res.render('detail', { name: "Coinbase", exchanges: data}));
});

router.get("/bitfinex", (req, res)=>{
    Exchange.find({"exchange": "bitfinex"}).then(data =>  res.render('detail', { name: "Bitfinex", exchanges: data}));
});

module.exports = router;